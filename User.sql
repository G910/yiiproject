-- Adminer 4.8.0 MySQL 5.5.5-10.3.29-MariaDB-0+deb10u1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE `recruitment_project`;

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(64) NOT NULL,
  `lastName` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(32) NOT NULL,
  `passwordChanged` date NOT NULL,
  `dateOfBirth` date NOT NULL,
  `registeredDatetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- 2021-06-27 07:53:34
