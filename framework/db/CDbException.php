<?php
/**
 * CDbException class file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright Copyright &copy; 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

/**
 * CDbException represents an exception that is caused by some DB-related operations.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @version $Id: CDbException.php 75 2008-10-13 21:26:19Z qiang.xue $
 * @package system.db
 * @since 1.0
 */
class CDbException extends CException
{
}