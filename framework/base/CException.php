<?php
/**
 * CException class file.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.yiiframework.com/
 * @copyright Copyright &copy; 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

/**
 * CException represents a generic exception for all purposes.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @version $Id: CException.php 314 2008-12-01 18:32:23Z qiang.xue $
 * @package system.base
 * @since 1.0
 */
class CException extends Exception
{
}

