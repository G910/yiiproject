<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $email;
	public $password;
	
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return [
			[
				'email, password',
				'required',
			],
			[
				'password',
				'authenticate',
			],
		];
	}
	
	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute, $params)
	{
		if (!$this->hasErrors())  // we only want to authenticate when no input errors
		{
			$identity = new CustomUserIdentity($this->email, $this->password);
			$identity->authenticate();
			switch ($identity->errorCode) {
				case CustomUserIdentity::ERROR_NONE:
					Yii::app()->user->login($identity, 0);
					break;
				case CustomUserIdentity::ERROR_USERNAME_INVALID:
					$this->addError('email', 'Username is incorrect.');
					break;
				default: // UserIdentity::ERROR_PASSWORD_INVALID
					$this->addError('password', 'Password is incorrect.');
					break;
			}
		}
	}
}