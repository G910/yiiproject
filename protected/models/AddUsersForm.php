<?php

/**
 * AddUsersForm class.
 * AddUsersForm is the data structure for keeping
 * user adding form data. It is used by the 'Users_Add' action of 'SiteController'.
 */
class AddUsersForm extends CFormModel
{
	public $excelFile;
	public $submit;
	
	/**
	 * Declares the validation rules.
	 * The rules state that file is required
	 */
	public function rules()
	{
		return [
			[
				'excelFile',
				'file',
			],
			[
				'excelFile',
				'parse',
			]
		];
	}
	
	/**
	 * Declares attribute labels.
	 * If not declared here, an attribute would have a label
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return [
			'excelFile' => 'XLS file',
		];
	}
	
	public function parse($attribute, $params)
	{
		if (!$this->hasErrors())  // we only want to proceed when no input errors
		{
			$file = CUploadedFile::getInstanceByName('AddUsersForm[excelFile]');
			if(!$file){
				$this->addError('excelFile', 'Upload file');
			}
			if($file->getType() !== "application/vnd.ms-excel"){
				$this->addError('excelFile', 'Wrong format. Upload XLS');
			}
			if(!$xls = SimpleXLS::parseFile($file->getTempName())){
				$this->addError('excelFile', 'Error reading file');
			}
			$rows = $xls->rows();
			$saved = [];
			foreach ($rows as $index => $row) {
				list($firstName, $lastName, $email, $dateOfBirth) = $row;
				if(!User::isExistsByEmail($email)){
					$passwordPlain = bin2hex(openssl_random_pseudo_bytes(4));
					$user = new User;
					$user->firstName = $firstName;
					$user->lastName = $lastName;
					$user->email = $email;
					$user->dateOfBirth = $dateOfBirth;
					$user->password = md5($passwordPlain);
					$user->passwordChanged = date('Y-m-d');
					$user->registeredDatetime = date('Y-m-d H:i:s');
					if($user->save()){
						$user->sendRegistrationMail($passwordPlain);
						$saved[] = $index + 1;
					}
				}
			}
			if(empty($saved)){
				$this->addError('excelFile', 'No user has been added to the database');
			}else{
				Yii::app()->session['savedRows'] = $saved;
			}
		}
	}
	
}