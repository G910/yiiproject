<?php

class User extends CActiveRecord
{
	public const ERROR_NONE           = -1;
	public const ERROR_WRONG_PASSWORD = 1;
	public const ERROR_SAME_PASSWORD  = 2;
	public const ERROR_OTHER          = 100;
	public        $id;
	public        $firstName;
	public        $lastName;
	public        $email;
	public        $password;
	public        $passwordChanged;
	public        $dateOfBirth;
	public        $registeredDatetime;
	
	public function __construct($attributes = [])
	{
		parent::__construct($attributes);
	}
	
	public static function isPasswordExpired()
	{
		return self::model()->findByAttributes("id", ['id' => Yii::app()->user->getId()]);
	}
	
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	public static function isExistsByEmail(string $email)
	{
		return self::model()->exists('email=:email', ['email' => $email]);
	}
	
	public static function changePassword(string $oldPassword, string $newPassword)
	{
		$user = self::model()->findByPk(Yii::app()->user->getId());
		if (!$user) {
			return self::ERROR_OTHER;
		}
		if ($user->password !== md5($oldPassword)) {
			return self::ERROR_WRONG_PASSWORD;
		}
		if ($user->password === md5($newPassword)) {
			return self::ERROR_SAME_PASSWORD;
		}
		$user->password = md5($newPassword);
		$user->passwordChanged = date('Y-m-d');
		if ($user->save()) {
			Yii::app()->session['passwordExpired'] = FALSE;
			return self::ERROR_NONE;
		}
	}
	
	public function sendRegistrationMail(string $passwordPlain)
	{
		Mailer::sendRegistrationMail($this->firstName, $this->lastName, $passwordPlain, $this->email);
	}
}