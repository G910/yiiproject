<?php

class CustomUserIdentity extends CUserIdentity
{
	private $_id;
	
	public function authenticate()
	{
		$record = User::model()->findByAttributes(['email' => $this->username]);
		if ($record === NULL) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} else {
			if ($record->password !== md5($this->password)) {
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
			} else {
				$this->_id = $record->id;
				if(strtotime($record->passwordChanged) + Yii::app()->getParams()->UserPasswordExpiration*24*60*60 <= strtotime(date("Y-m-d"))){
					Yii::app()->session['passwordExpired'] = true;
				}
				$this->errorCode = self::ERROR_NONE;
			}
		}
		return !$this->errorCode;
	}
	
	public function getId()
	{
		return $this->_id;
	}
}