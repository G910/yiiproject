<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class ChangePasswordForm extends CFormModel
{
	public $oldPassword;
	public $newPassword;
	public $newPassword_repeat;
	
	/**
	 * Declares the validation rules.
	 * The rules state that old password, new password and new password repeated are required,
	 * and password needs to be changed.
	 */
	public function rules()
	{
		return [
			[
				'oldPassword, newPassword, newPassword_repeat',
				'required',
			],
			[
				'newPassword',
				'compare',
			],
			[
				'newPassword',
				'changePassword',
			],
		];
	}
	
	public function attributeLabels()
	{
		return [
			'oldPassword' => 'Current password',
			'newPassword' => 'New password',
			'newPassword_repeat' => 'Repeat new password',
		];
	}
	
	/**
	 *
	 */
	public function changePassword($attribute, $params)
	{
		if (!$this->hasErrors())  // we only want to proceed when no input errors
		{
			$response = User::changePassword($this->oldPassword, $this->newPassword);
			var_dump($response);
			switch ($response) {
				case User::ERROR_NONE:
					return true;
				case User::ERROR_WRONG_PASSWORD:
					$this->addError('oldPassword', 'Password is incorrect.');
					break;
				case User::ERROR_SAME_PASSWORD:
					$this->addError('newPassword', 'New password is the same as current.');
					break;
				default:
					$this->addError('', 'Unknown error. Please try again');
					break;
			}
		}
	}
}