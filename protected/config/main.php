<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return [
	'basePath'   => __DIR__ . DIRECTORY_SEPARATOR . '..',
	'name'       => 'Recruitment Project',
	
	// autoloading model and component classes
	'import'     => [
		'application.models.*',
		'application.components.*',
		'application.vendors.SimpleXLS.SimpleXLS',
		'application.vendors.Mailer.Mailer',
	],
	
	'params' => [
		'rootURL' => 'https://example.com',
		'UserPasswordExpiration' => 30,
		'SMTP_host' => 'smtp.example.com',
		'SMTP_port' => 587,
		'SMTP_smtpsecure' => 'tls',
		'SMTP_username' => 'registration@example.com',
		'SMTP_password' => 'veryhardtoguesspassword',
		'SMTP_name' => 'Registration',
		'Registration_Title' => 'You have been registered'
	],
	
	// application components
	'components' => [
		'user' => [
			// enable cookie-based authentication
			'allowAutoLogin' => TRUE,
		],
		'db'   => [
			'class'            => 'CDbConnection',
			'connectionString' => 'mysql:host=localhost;dbname=recruitment_project',
			'username'         => 'recruitment_project',
			'password'         => 'veryhardtoguesspassword',
			'emulatePrepare'   => TRUE,
		],
	],
];