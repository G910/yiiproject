<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
		<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
				<meta name="language" content="en"/>
				<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"/>
				<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"/>
				<title><?php echo $this->pageTitle; ?></title>
		</head>
		
		<body>
				<div id="page">
						
						<div id="header">
								<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
								<div id="mainmenu">
										<?php $this->widget(
														'application.components.MainMenu',
														[
																		'items' => [
																						[
																										'label'   => 'Home',
																										'url'     => ['site/index'],
																										'visible' => !Yii::app()->user->isGuest,
																						],
																						[
																										'label'   => 'Users list',
																										'url'     => ['site/usersList'],
																										'visible' => !Yii::app()->user->isGuest,
																						],
																						[
																										'label'   => 'Add users',
																										'url'     => ['site/usersAdd'],
																										'visible' => !Yii::app()->user->isGuest,
																						],
																						[
																										'label'   => 'Login',
																										'url'     => ['site/login'],
																										'visible' => Yii::app()->user->isGuest,
																						],
																						[
																										'label'   => 'Logout',
																										'url'     => ['site/logout'],
																										'visible' => !Yii::app()->user->isGuest,
																						],
																		],
														]
										); ?>
								</div><!-- mainmenu -->
						</div><!-- header -->
						
						<div id="content">
								<?php echo $content; ?>
						</div><!-- content -->
						
						<div id="footer">
								Copyright &copy; <?php echo date("Y"); ?> by Beniamin Kowol.<br/>
								All Rights Reserved.<br/>
								<?php echo Yii::powered(); ?>
						</div><!-- footer -->
				
				</div><!-- page -->
		</body>

</html>