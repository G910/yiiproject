<h1>Login</h1>

<div class="yiiForm">
		<?php echo CHtml::form(); ?>
		
		<?php echo CHtml::errorSummary($user); ?>
		
		<div class="simple">
				<?php echo CHtml::activeLabel($user, 'email'); ?>
				<?php echo CHtml::activeTextField($user, 'email') ?>
		</div>
		
		<div class="simple">
				<?php echo CHtml::activeLabel($user, 'password'); ?>
				<?php echo CHtml::activePasswordField($user, 'password') ?>
				<p class="hint">
						Hint: You may login with <tt>admin/admin</tt>.
				</p>
		</div>
		
		
		<div class="action">
				<?php echo CHtml::submitButton('Login'); ?>
		</div>
		
		</form>
</div><!-- yiiForm -->