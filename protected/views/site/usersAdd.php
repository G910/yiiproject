<h2>
		Add users
</h2>

<div class="yiiForm">
		<?php echo CHtml::form('', 'post', ['enctype' => 'multipart/form-data']); ?>
		
		<?php echo CHtml::errorSummary($form); ?>
		
		<div class="simple">
				<?php echo CHtml::activeLabel($form, 'excelFile'); ?>
				<?php echo CHtml::activeFileField($form, 'excelFile') ?>
				<p class="hint ">
						Hint: XLS file should contain users in separate rows. Columns order: First name, Last name, E-mail, Date of birth
				</p>
		</div>
		
		<?php echo CHtml::activeHiddenField($form, 'submit') ?>
		
		<div class="action">
				<?php echo CHtml::submitButton('Upload'); ?>
		</div>
		</form>
</div>