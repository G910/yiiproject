<h2>
		List of users
</h2>

<table style="width: 100%; text-align: center;">
		<thead>
				<th scope="col">ID</th>
				<th scope="col">First name</th>
				<th scope="col">Last name</th>
				<th scope="col">E-mail</th>
				<th scope="col">Date of birth</th>
				<th scope="col">Date of registration</th>
		</thead>
		<tbody>
				<?php
				foreach ($users as $user) {
						?>
						<tr>
								<th scope="row">
										<?php echo $user->id; ?>
								</th>
								<td>
										<?php echo $user->firstName; ?>
								</td>
								<td>
										<?php echo $user->lastName; ?>
								</td>
								<td>
										<?php echo $user->email; ?>
								</td>
								<td>
										<?php echo $user->dateOfBirth; ?>
								</td>
								<td>
										<?php echo $user->registeredDatetime; ?>
								</td>
						</tr>
						<?php
				}
				?>
		</tbody>
</table>