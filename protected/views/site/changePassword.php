<h2>Change password</h2>

<form class="yiiForm">
		<?php echo CHtml::form(); ?>
		
		<?php echo CHtml::errorSummary($form); ?>
		
		<div class="simple">
				<?php echo CHtml::activeLabel($form, 'oldPassword'); ?>
				<?php echo CHtml::activePasswordField($form, 'oldPassword') ?>
		</div>
		
		<div class="simple">
				<?php echo CHtml::activeLabel($form, 'newPassword'); ?>
				<?php echo CHtml::activePasswordField($form, 'newPassword') ?>
		</div>
		
		<div class="simple">
				<?php echo CHtml::activeLabel($form, 'newPassword_repeat'); ?>
				<?php echo CHtml::activePasswordField($form, 'newPassword_repeat') ?>
		</div>
		
		
		<div class="action">
				<?php echo CHtml::submitButton('Change password'); ?>
		</div>

</form>
</div>