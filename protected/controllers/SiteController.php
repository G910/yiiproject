<?php

class SiteController extends CController
{
	
	public function filters()
	{
		return [
			'accessControl',
			'isPasswordExpired',
		];
	}
	
	public function filterIsPasswordExpired(CFilterChain $filterChain)
	{
		if (@Yii::app()->session['passwordExpired'] === TRUE && Yii::app()->controller->action->id !== 'changePassword') {
			$this->redirect($this->createUrl('site/changePassword'));
		}
		$filterChain->run();
	}
	
	public function accessRules()
	{
		return [
			[
				'deny',
				'actions' => [
					'logout',
					'usersAdd',
					'usersList',
					'changePassword',
				],
				'users'   => ['?'],
			],
			[
				'deny',
				'actions' => ['login'],
				'users'   => ['@'],
			],
		];
	}
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$this->render('index');
	}
	
	/**
	 * Displays a login form to login a user.
	 */
	public function actionLogin()
	{
		$user = new LoginForm;
		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$user->attributes = $_POST['LoginForm'];
			// validate user input and redirect to previous page if valid
			if ($user->validate()) {
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		// display the login form
		$this->render('login', ['user' => $user]);
	}
	
	/**
	 * Logout the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	/**
	 * Allow logged in user to upload xlsx file containing users to add
	 */
	public function actionUsersAdd()
	{
		$form = new AddUsersForm;
		if (isset($_POST['AddUsersForm'])) {
			if ($form->validate()) {
				$this->redirect($this->createUrl('site/usersAdded'));
			}
		}
		$this->render('usersAdd', ['form' => $form]);
	}
	
	public function actionUsersAdded()
	{
		if(Yii::app()->session['savedRows'] === false){
			$this->redirect($this->createUrl('site/usersAdd'));
		}
		$savedRows = Yii::app()->session['savedRows'];
		Yii::app()->session['savedRows'] = false;
		$this->render('usersAdded', ['savedRows' => implode(', ', $savedRows), 'savedRowsCount' => count($savedRows)]);
	}
	
	/**
	 * Allow logged in user to view list of other users
	 */
	public function actionUsersList()
	{
		$users = User::model()->findAll('id != :user_id', ['user_id' => Yii::app()->user->getId()]);
		$this->render('usersList', ['users' => $users]);
	}
	
	/**
	 * Allow user to change password
	 */
	public function actionChangePassword()
	{
		$form = new ChangePasswordForm;
		if (isset($_POST['ChangePasswordForm'])) {
			$form->attributes = $_POST['ChangePasswordForm'];
			if ($form->validate()) {
				$this->redirect($this->createUrl('site/changedPassword'));
			}
		}
		$this->render('changePassword', ['form' => $form]);
	}
	
	/**
	 * Show confirmation when password changed
	 */
	public function actionChangedPassword()
	{
		$this->render('changedPassword');
	}
}