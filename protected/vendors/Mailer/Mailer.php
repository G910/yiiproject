<?php

class Mailer
{
	public static function sendRegistrationMail(string $firstName, string $lastName, string $passwordPlain, string $email)
	{
		Yii::import('application.extensions.phpmailer.JPhpMailer');
		$message_content = file_get_contents(
			Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'mail' . DIRECTORY_SEPARATOR . 'registration.php'
		);
		$template_params = [
			'firstName' => $firstName,
			'lastName' => $lastName,
			'passwordPlain' => $passwordPlain,
			'loginURL' => Yii::app()->getParams()->rootURL . Yii::app()->createUrl('site/login'),
		];
		foreach ($template_params as $param => $value) {
			$message_content = str_replace(':' . $param, $value, $message_content);
		}
		$mail = new JPhpMailer;
		$mail->IsSMTP();
		$mail->Host = Yii::app()->getParams()->SMTP_host;
		$mail->SMTPAuth = TRUE;
		$mail->SMTPSecure = Yii::app()->getParams()->SMTP_smtpsecure;
		$mail->Port = Yii::app()->getParams()->SMTP_port;
		$mail->CharSet = 'UTF-8';
		$mail->Username = Yii::app()->getParams()->SMTP_username;
		$mail->Password = Yii::app()->getParams()->SMTP_password;
		$mail->From = Yii::app()->getParams()->SMTP_username;
		$mail->FromName = Yii::app()->getParams()->SMTP_name;
		$mail->Subject = Yii::app()->getParams()->Registration_Title;
		$mail->AltBody = 'To view this message, please use an HTML compatible email viewer!';
		$mail->MsgHTML($message_content);
		$mail->AddAddress($email, $firstName . ' ' . $lastName);
		$mail->Send();
	}
}